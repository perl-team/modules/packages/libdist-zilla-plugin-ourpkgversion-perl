libdist-zilla-plugin-ourpkgversion-perl (1:0.21-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libdist-zilla-perl.
    + libdist-zilla-plugin-ourpkgversion-perl: Drop versioned constraint on
      libdist-zilla-perl in Depends.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 20 Nov 2022 15:48:17 +0000

libdist-zilla-plugin-ourpkgversion-perl (1:0.21-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.21

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Sat, 03 Aug 2019 12:56:47 +0530

libdist-zilla-plugin-ourpkgversion-perl (1:0.20-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.20
  * Add dependency in d/control
  * Bump compat to 12
  * Bump Standards-Version to 4.4.0

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Wed, 17 Jul 2019 01:57:15 +0530

libdist-zilla-plugin-ourpkgversion-perl (1:0.14-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.14.
  * Update upstream contact.
  * Update years of upstream copyright.
  * Make (build) dependency on libdist-zilla-perl versioned.
  * Lowercase short description.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.

 -- gregor herrmann <gregoa@debian.org>  Fri, 08 Feb 2019 16:19:23 +0100

libdist-zilla-plugin-ourpkgversion-perl (1:0.12-1) unstable; urgency=medium

  * Import upstream version 0.12.
    + Add new build dependencies for test suite:
      libtest-minimumversion-perl, libtest-perl-critic-perl, and
      libtest-portability-files-perl.
  * Declare compliance with Debian Policy 4.1.0.

 -- Axel Beckert <abe@debian.org>  Sun, 24 Sep 2017 01:18:00 +0200

libdist-zilla-plugin-ourpkgversion-perl (1:0.10-1) unstable; urgency=medium

  * Import upstream version 0.10

 -- Axel Beckert <abe@debian.org>  Sat, 25 Jun 2016 10:21:38 +0200

libdist-zilla-plugin-ourpkgversion-perl (1:0.08-2) unstable; urgency=medium

  * Team upload.
  * Add build and runtime dependency on libpath-class-perl.
    Thanks to Chris Lamb for the bug report. (Closes: #825366)
  * Add debian/upstream/metadata.
  * Update years of upstream copyright.
  * Drop version constraint from libtest-version-perl.

 -- gregor herrmann <gregoa@debian.org>  Thu, 26 May 2016 18:05:14 +0200

libdist-zilla-plugin-ourpkgversion-perl (1:0.08-1) unstable; urgency=medium

  [ Axel Beckert ]
  * Fix grammar in package description. Thanks Damyan Ivanov!
  * Fix pkg-perl-specific lintian warning no-module-name-in-description
  * Import new upstream release 0.08
    + Requiring epoch since dpkg considers 0.005001 greater than 0.08.
  * Declare compliance with Debian Policy 3.9.8. (No changes needed.)
  * Mark package as autopkgtestable. Add directory corpus to smoke-files.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

 -- Axel Beckert <abe@debian.org>  Thu, 26 May 2016 00:23:24 +0200

libdist-zilla-plugin-ourpkgversion-perl (0.005001-1) unstable; urgency=low

  * Initial Release (Closes: #757245)

 -- Axel Beckert <abe@debian.org>  Wed, 06 Aug 2014 18:34:43 +0200
